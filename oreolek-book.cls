% Declare that this style file requires at least LaTeX version 2e.
\NeedsTeXFormat{LaTeX2e}

% Provide the name of your page, the date it was last updated, and a comment about what it's used for
\ProvidesPackage{oreolek-book}[2012/10/09 Oreolek's custom XeTeX style for books]

\DeclareOption*{%
  \PassOptionsToClass{\CurrentOption}{memoir}% or book or whatever
}

% Now we'll execute any options passed in
\ProcessOptions\relax

% Instead of defining each and every little detail required to create a new document class,
% you can base your class on an existing document class.
\LoadClass{memoir}% or book or whatever you class is closest to

\RequirePackage{fontspec}
\RequirePackage{xunicode}
\RequirePackage{xltxtra}
\RequirePackage{polyglossia}
\RequirePackage{pifont}
\setdefaultlanguage{russian}
\defaultfontfeatures{Mapping=tex-text, Scale=MatchLowercase}
\setmainfont{PT Serif}
\setmonofont{PT Mono}
\setmathrm{PT Serif}
\newfontfamily{\cyrillicfont}{PT Serif}
\newfontinstance\scshape[Letters=SmallCaps, Numbers=Uppercase]{PT Serif Caption}
\RequirePackage{hyperref}
\RequirePackage{indentfirst}
\RequirePackage[cm]{fullpage}
\RequirePackage{misccorr}
\RequirePackage{eqell}
\pagestyle{empty}
\setcounter{secnumdepth}{0}
\setcounter{tocdepth}{2}
\author{Александр Яковлев}
\newcommand{\ornament}{\begin{center}\ding{167}\end{center}}
\endinput
